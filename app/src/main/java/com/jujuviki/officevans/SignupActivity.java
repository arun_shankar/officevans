package com.jujuviki.officevans;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

/**
 * Created by arunshankar on 05/06/15.
 */
public class SignupActivity extends Activity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    CallbackManager callbackManager;

    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    private static final int RC_SIGN_IN = 0;

    /**
     * A flag indicating that a PendingIntent is in progress and prevents us
     * from starting further intents.
     */
    private boolean mIntentInProgress;

    private boolean mSignInClicked;

    private ConnectionResult mConnectionResult;
    private Button googleLogin,fblogin;

    TextView tnc;

    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO:add check if user is alraedy loggedin by checking if user object is now null

        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(
                                    JSONObject object,
                                    GraphResponse response) {
                                // Application code
                                try {
                                    String email = object.getString("email");
                                    String name = object.getString("name");
                                    String facebookId = object.getString("id");
                                    String imageUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");
                                    createNewFBUser(email, name, imageUrl ,facebookId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,picture");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                //Logg.m("MAIN", "Response  : Facebook Graph = Cancelled");
            }

            @Override
            public void onError(FacebookException e) {
                //Logg.m("MAIN", "Response  : Facebook Graph = Error=" + e.toString());
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addOnConnectionFailedListener(this).addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();

        setContentView(R.layout.activity_signup);

        fblogin = (Button) findViewById(R.id.fblogin);

        fblogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Logg.m("MAIN", "Requested for Facebook Login");
                LoginManager.getInstance().logInWithReadPermissions(SignupActivity.this, Arrays.asList("email", "public_profile"));
            }
        });

        googleLogin = (Button) findViewById(R.id.googleLogin);

        googleLogin.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                signInWithGplus();
            }
        });

        /*tnc = (TextView) findViewById(R.id.tnc);
        String text = "<a href=\"http://tinystep.in/tnc.html\">Terms and Conditions </a> and <a href=\"http://tinystep.in/privacy.html\">Privacy Policy </a> ";
        tnc.setMovementMethod(LinkMovementMethod.getInstance());
        tnc.setText(Html.fromHtml(text));*/

        //TODO:remove this shit
//        try{
//            if(Settings.bypassLogin) loginWithEmail("sabertoothmaurice4@gmail.com", "Maurice", "12123", null, true);//todo:remove this line
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

    private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onConnected(Bundle bundle) {
        mSignInClicked = false;
        //Utils.showDebugToast(this, "User is connected!");

        // Get user's information
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        mLocationRequest.setInterval(1000 * 60 * 5);

        LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient, mLocationRequest, this);
        getGProfileInformation();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    private void signInWithGplus() {
        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, 0).show();
            return;
        }

        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = connectionResult;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }

    }

    private void getGProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi
                        .getCurrentPerson(mGoogleApiClient);
                String personName = currentPerson.getDisplayName();
                String personGooglePlusProfile = currentPerson.getUrl();
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                String imageUrl =  currentPerson.getImage().getUrl();
                createNewGPlusUser(email, personName, imageUrl);
            } else {
                //Utils.showDebugToast(getApplicationContext(), "Person information is null");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void onResume() {
        super.onResume();

        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }

    private void createShortcutIcon(){

        Intent shortcutIntent = new Intent(getApplicationContext(), SignupActivity.class);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shortcutIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        Intent addIntent = new Intent();
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, "TinyStep");
        addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, Intent.ShortcutIconResource.fromContext(getApplicationContext(), R.mipmap.ic_launcher));
        addIntent.setAction("com.android.launcher.action.INSTALL_SHORTCUT");
        getApplicationContext().sendBroadcast(addIntent);
    }

    void createNewGPlusUser(final String email, String name,String imageUrl) {
        final ParseUser user = new ParseUser();
        user.setUsername(email);
        user.setPassword("my pass");
        user.setEmail(email);
        user.put("name", name);
        user.put("Photo",imageUrl);
        signInParseUser(user, email);
    }

    void createNewFBUser(String email, String name, String imageUrl,String fbProfile) {
        ParseUser user = new ParseUser();
        user.setUsername(email);
        user.setPassword("my pass");
        user.setEmail(email);
        user.put("name", name);
        user.put("fbProfile", fbProfile);
        user.put("Photo",imageUrl);
        this.signInParseUser(user, email);
    }

    void signInParseUser(final ParseUser user, final String email) {

        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("ARUN SHANKAR", "Created user");
                    // Hooray! Let them use the app now.
                    login(email);
                } else {
                    Log.d("ARUN SHANKAR", "Failed Creating user");
                    e.printStackTrace();
                    if (e.getMessage().contains("already taken")) {
                        login(email);
                    }
                    // Sign up didn't succeed. Look at the ParseException
                    // to figure out what went wrong
                }
            }
        });
    }

    void login(final String email) {
        ParseUser.logInInBackground(email, "my pass", new LogInCallback() {
            public void done(ParseUser user, ParseException e) {
                if (user != null) {
                    // Hooray! The user is logged in.
                    Log.d("ARUN SHANKAR", "Login successful");
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("username", email);
                    editor.putString("buyerName", user.getString("name"));
                    editor.putBoolean("isSignedUp", true);
                    editor.commit();
                    Intent intent = new Intent(SignupActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                    finish();

                } else {
                    Log.d("ARUN SHANKAR", "Login failed");
                    SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("username", email);
                    editor.putString("buyerName", email);
                    editor.putBoolean("isSignedUp", true);
                    editor.commit();
                    Intent intent = new Intent(SignupActivity.this,RegistrationActivity.class);
                    startActivity(intent);
                    finish();
                    // Signup failed. Look at the ParseException to see what happened.
                }
            }
        });
    }

    @Override
    public void onLocationChanged(Location location) {
        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("currentLatitude", String.valueOf(location.getLatitude()));
        editor.putString("currentLongitude", String.valueOf(location.getLongitude()));
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());
        try {
            addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
            String address = addresses.get(0).getSubLocality() + "," + addresses.get(0).getAddressLine(1);
            editor.putString("currentArea", address);
        } catch (IOException e) {
            e.printStackTrace();
        }


        editor.commit();
    }


    //MAIN USER UPSERT
    /** ANY LOGIN METHOD SHOULD IN THE END CALL THIS FUNCTION
     *
     * This first sends server email to get userId through user object,
     * then updates in local and then syncs with server*/
    /*private void loginWithEmail(final String email, final String name, final String facebookId, final String imageUrl, final boolean isFb) throws JSONException {
        Utils.showDebugToast(getApplicationContext(), "logging in as " + email);
        String url = Router.User.upsertUserEmailComplete(email);
        JSONObject jsonObject = new JSONObject();
        Logg.m("MAIN", "Checking Email : " + email);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    Logg.m("MAIN", "Response : Email check = " + response.toString());
                    if(response.getString("status").equalsIgnoreCase("success")) {

                        //Gets if user has previous registration data
                        MainApplication.getInstance().data.refillCompleteData(response.getJSONObject("result"));


                        //Update user data
                        Logg.m("MAIN", "Saving add-on data");
                        Data data = MainApplication.getInstance().data;
                        data.userMain.name = name;
                        data.userMain.email = email;
                        data.userMain.imageUrl = imageUrl;
                        if (isFb) {
                            data.userMain.facebookId = facebookId;
                        }

                        //saves in local and then sync with server
                        data.userMain.saveUserDataLocally();
                        data.userMain.serverSyncPush();

                        //add flurry event
                        Map<String, String> articleParams = new HashMap<String, String>();
                        articleParams.put("IMEI", Utils.getImei());
                        articleParams.put("Userid", Utils.userId());
                        articleParams.put("Email", email);
                        FlurryAgent.logEvent("Signup", articleParams);

                        createShortcutIcon();//Todo: add shortcut status in prefs
                        loginToMainApp();
                    }else{
                        Utils.showDebugToast(getApplicationContext(),"error in creating user");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("Check email failure", "" + error.getLocalizedMessage());
            }
        });

        MainApplication.getInstance().getRequestQueue().add(jsonObjectRequest);

    }

    //This is the main entrance to the Main APP
    private void loginToMainApp(){
        Data data = MainApplication.getInstance().data;
        if(data.userMain.phone.equals("null")){
            Intent intent = new Intent(SignupActivity.this, AddPhoneNumberActivity.class);
            startActivity(intent);
            finish();
        }else if(data.kidHandler.kidsArray.size()==0){
            Intent intent = new Intent(SignupActivity.this, AddInitialKidActivity.class);
            startActivity(intent);
            finish();
        }else{
            Intent intent1 = new Intent(SignupActivity.this, VaccinationActivity.class);
            startActivity(intent1);
            finish();
        }

    }*/

}
