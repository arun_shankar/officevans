package com.jujuviki.officevans;

import android.app.Activity;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by arunshankar on 05/06/15.
 */
public class RegistrationActivity extends Activity implements CompoundButton.OnCheckedChangeListener {

    double lat,lng;
    double destLat,destLng;
    ImageView pickupLocation,dropLocation;
    TextView pickupAddress,nameText,emailText,companyId,dropAddress;
    EditText mobileEditText;
    TextView fromTime,toTime;

    int hour,minute;

    CheckBox sunday,monday,tuesday,wednesday,thursday,friday,saturday;
    boolean[] days = new boolean[7];

    CheckBox homeToOffice,officeToHome;

    Button registerButton;

    Spinner dropAddressSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        destLat = 12.927837;
        destLng = 77.633021;
        pickupLocation = (ImageView) findViewById(R.id.pickupLocation);
        dropLocation = (ImageView) findViewById(R.id.dropLocation);
        mobileEditText = (EditText) findViewById(R.id.mobileEditText);
        registerButton = (Button) findViewById(R.id.registerButton);
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.mipmap.ic_launcher)
                .showImageForEmptyUri(R.mipmap.ic_launcher)
                .showImageOnFail(R.mipmap.ic_launcher)
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .build();
        lat  = Double.parseDouble(getApplicationContext().getSharedPreferences("MyPref", 0).getString("currentLatitude","0.0"));
        lng  = Double.parseDouble(getApplicationContext().getSharedPreferences("MyPref", 0).getString("currentLongitude","0.0"));
        String url = "http://maps.google.com/maps/api/staticmap?center=" + lat + "," + lng + "&zoom=14&size=400x200&sensor=false&markers=color:blue|label:S|" + lat + "," + lng;
        ImageLoader.getInstance().displayImage(url, pickupLocation, options, null);

        pickupAddress = (TextView) findViewById(R.id.pickUpAddress);
        nameText = (TextView) findViewById(R.id.nameText);
        emailText = (TextView) findViewById(R.id.emailText);
        companyId = (TextView) findViewById(R.id.companyId);
        dropAddress = (TextView) findViewById(R.id.dropAddress);

        fromTime = (TextView) findViewById(R.id.fromTime);
        toTime = (TextView) findViewById(R.id.toTime);

        sunday = (CheckBox) findViewById(R.id.sunday);
        monday = (CheckBox) findViewById(R.id.monday);
        tuesday = (CheckBox) findViewById(R.id.tuesday);
        wednesday = (CheckBox) findViewById(R.id.wednesday);
        thursday = (CheckBox) findViewById(R.id.thursday);
        friday = (CheckBox) findViewById(R.id.friday);
        saturday = (CheckBox) findViewById(R.id.saturday);

        homeToOffice = (CheckBox) findViewById(R.id.homeToOffice);
        officeToHome = (CheckBox) findViewById(R.id.officeToHome);

        fromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(RegistrationActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        fromTime.setText(hourOfDay+":"+minute);
                    }
                },hour,minute,true);
                timePickerDialog.show();
            }
        });


        toTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimePickerDialog timePickerDialog = new TimePickerDialog(RegistrationActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        toTime.setText(hourOfDay + ":" + minute);
                    }
                }, hour, minute, true);
                timePickerDialog.show();
            }
        });

        nameText.setText("Name : " + getApplicationContext().getSharedPreferences("MyPref", 0).getString("buyerName", "Me"));
        emailText.setText("Email : " + getApplicationContext().getSharedPreferences("MyPref", 0).getString("username","Me"));

        pickupAddress.setText(getApplicationContext().getSharedPreferences("MyPref", 0).getString("currentArea","Here"));

        url = "http://maps.google.com/maps/api/staticmap?center=" + destLat + "," + destLng + "&zoom=14&size=400x200&sensor=false&markers=color:blue|label:S|" + destLat + "," + destLng;
        ImageLoader.getInstance().displayImage(url, dropLocation, options, null);

        dropAddressSpinner = (Spinner) findViewById(R.id.dropAddressSpinner);

        dropAddressSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(position==1)
                    dropLocation.setVisibility(View.VISIBLE);
                else
                    dropLocation.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveInParser();
            }
        });

    }

    private void saveInParser() {
        Intent intent = new Intent(RegistrationActivity.this,OptionsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Connect the client.
    }

    @Override
    protected void onStop() {
        // Disconnecting the client invalidates it.
        super.onStop();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.sunday:
                if(!isChecked)
                    days[0]=false;
                else
                    days[0]=true;
            break;
            case R.id.monday:
                if(!isChecked)
                    days[1]=false;
                else
                    days[1]=true;
                break;
            case R.id.tuesday:
                if(!isChecked)
                    days[2]=false;
                else
                    days[2]=true;
                break;
            case R.id.wednesday:
                if(!isChecked)
                    days[3]=false;
                else
                    days[3]=true;
                break;
            case R.id.thursday:
                if(!isChecked)
                    days[4]=false;
                else
                    days[4]=true;
                break;
            case R.id.friday:
                if(!isChecked)
                    days[5]=false;
                else
                    days[5]=true;
                break;
            case R.id.saturday:
                if(!isChecked)
                    days[6]=false;
                else
                    days[6]=true;
                break;
        }
    }
}
