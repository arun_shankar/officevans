package com.jujuviki.officevans;

import android.app.Activity;
import android.os.Bundle;
import android.widget.ListView;

/**
 * Created by arunshankar on 06/06/15.
 */
public class OptionsActivity extends Activity{

    ListView optionsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_options);
        optionsList = (ListView) findViewById(R.id.optionsList);
    }
}
